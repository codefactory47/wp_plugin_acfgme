<?php
/*
Plugin Name: Cf47 Advanced Custom Fields: Advanced Google Map
Plugin URI: http://codefactory47.com
Description: Improved ACF map field
Version: 1.0.0
OriginalAuthor: CodeFish
Author URI: http://codefactory47.com
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Text Domain: acf-gme
Domain Path: /lang
*/

class acf_field_google_map_extended_plugin {
  
  const version = '1.0.0';

  function __construct() {
    add_action('plugins_loaded', array($this, 'plugins_loaded') );
    add_action('acf/include_field_types', array($this, 'include_field_types'));
  }

  function plugins_loaded() {
    load_plugin_textdomain('acf-gme', false, dirname( plugin_basename(__FILE__) ) . '/lang/' );
  }


  function include_field_types($version) {// $version = 5 and can be ignored until ACF6 exists
    include_once('acf-google-map-extended-base.php');
    include_once('acf-google-map-extended-v5.php');
  }
}

new acf_field_google_map_extended_plugin();
?>